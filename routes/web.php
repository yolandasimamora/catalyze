<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$latest_news = DB::table('news')->limit(3)->get();
    return view('welcome', ['latest_news' => $latest_news]);
});
Route::get('/news_detail/{id}', function ($id) {
    $detail = DB::table('news')
    			->where('id', $id)
    			->get();
    $result = $detail->toArray(); 
    $users = DB::table('users')
    			->where('id', $result[0]->user_id)
    			->get();
    $user = $users->toArray();
    return view('news_detail', ['result' => $result, 'user' => $user]);
});

//News Index Admin
Route::resource('news',NewsController::class)->middleware('auth');
Route::resource('users',UsersController::class)->middleware('auth');
Route::get('summernote-image-upload', [NewsController::class, 'show']);
Route::post('post-summernote-image-upload', [NewsController::class, 'store']);
Auth::routes();
Route::get('admin', 'AdminController@index')->name('admin');