<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\Hash;
use Session;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Users::latest()->paginate(5);

        return view('admin.users.index',compact('user'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $validated = $request->validate([
                    'name' => 'required',
                    'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'email' => 'required',
                    'password' => 'required',
                ]);
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/upload/users');
                $image->move($destinationPath, $input['imagename']);
                //save users
                $users = new Users();
                $users->name = $request->get('name');
                $users->email = $request->get('email');
                $users->password =  Hash::make($request->get('password'));
                $users->description = $request->get('desc');
                $users->image = $input['imagename'];
                $users->save();
                return redirect()->route('users.index')
                ->with('success','Users created successfully.');
            } abort(500, 'Could not upload image :(');
        } else {
            $validated = $request->validate([
                'name' => 'required',
                'email' => 'required',
                'password' => 'required',
            ]);
            $detail = $request->detail;
            $users = new Users();
            $users->name = $request->get('name');
            $users->email = $request->get('email');
            $users->password =  Hash::make($request->get('password'));
            $users->description = $request->get('desc');
            $users->save();
            return redirect()->route('users.index')
            ->with('success','News created successfully.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Users $user)
    {
        return view('admin.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Users $user)
    {
        return view('admin.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Users $user)
    {
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $validated = $request->validate([
                    'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/upload/users');
                $image->move($destinationPath, $input['imagename']);
                $user->image = $input['imagename'];
                $user->password =  Hash::make($request->get('password'));
                $user->update($request->all());
                return redirect()->route('users.index')
                ->with('success','Users created successfully.');
            } abort(500, 'Could not upload image :(');
        } else {
            $user->password =  Hash::make($request->get('password'));
            $user->update($request->all());
        }

        return redirect()->route('users.index')
            ->with('success','Users updated successfully');
    }


    public function delete_img($image){
        // hapus file
        $gambar = Gambar::where('image',$image)->first();
        File::delete('upload/'.$gambar->file);
     
        // hapus data
        Gambar::where('image',$image)->delete();
     
        return redirect('users.index')->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Users $user)
    {
        $user->delete();

        return redirect()->route('users.index')
            ->with('success','Users deleted successfully');
    }
}