<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use App\Event;
use Session;
use Auth;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $news = News::latest()->paginate(5);

        return view('admin.news.index',compact('news'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $validated = $request->validate([
                    'title' => 'required',
                    'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'detail' => 'required',
                ]);
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/upload');
                $image->move($destinationPath, $input['imagename']);

                $detail = $request->detail;
                $dom = new \DomDocument();
                $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
                $images = $dom->getElementsByTagName('img');
                foreach($images as $k => $img){
                    $data = $img->getAttribute('src');
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name= "/upload/" . time().$k.'.png';
                    $path = public_path() . $image_name;
                    file_put_contents($path, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $image_name);
                }
                $detail = $dom->saveHTML();
                $article = new News();
                $email = $request->session()->get('email');
                $collection = DB::table('users')->where('email', $email)->get();
                $result = $collection->toArray();  
                $article->user_id = json_encode($result[0]->id);
                $article->title = $request->get('title');
                $article->detail = $detail;
                $article->image = $input['imagename'];
                $article->save();
                return redirect()->route('news.index')
                ->with('success','News created successfully.');
            } abort(500, 'Could not upload image :(');
        } else {
            $validated = $request->validate([
                'title' => 'required',
                'detail' => 'required',
            ]);
            $detail = $request->detail;
            $dom = new \DomDocument();
            $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $dom->getElementsByTagName('img');
            foreach($images as $k => $img){
                $data = $img->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);
                $image_name= "/upload/" . time().$k.'.png';
                $path = public_path() . $image_name;
                file_put_contents($path, $data);
                $img->removeAttribute('src');
                $img->setAttribute('src', $image_name);
            }
            $detail = $dom->saveHTML();
            $article = new News;
            $email = $request->session()->get('email');
            $collection = DB::table('users')->where('email', $email)->get();
            $result = $collection->toArray();       
            $article->user_id = json_encode($result[0]->id);
            $article->title = $request->title;
            $article->detail = $detail;
            $article->save();
            return redirect()->route('news.index')
            ->with('success','News created successfully.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(News $news)
    {
        return view('admin.news.show',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(News $news)
    {
        return view('admin.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, News $news)
    {
        $news->update($request->all());

        return redirect()->route('news.index')
            ->with('success','News updated successfully');
    }


    public function delete_img($image){
        // hapus file
        $gambar = Gambar::where('image',$image)->first();
        File::delete('upload/'.$gambar->file);
     
        // hapus data
        Gambar::where('image',$image)->delete();
     
        return redirect('news.index')->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(News $news)
    {
        $news->delete();

        return redirect()->route('news.index')
            ->with('success','News deleted successfully');
    }
}