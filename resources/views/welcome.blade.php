@extends('layouts.layouts_fe')
    <header class="site_header">
        <div class="container">
            <div class="top-header">
                <div class="logo-container">
                    <a href="{{ url('/') }}"><img src="{{asset("logo_catalyze.png")}}" alt="catalyze"></a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#catalyzemenu" aria-controls="catalyzemenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button> 
                </div>
                <div class="site-menu_container"> 
                    <nav class="navbar navbar-expand-lg  ">   
                        <div class="collapse navbar-collapse mainmenu" id="catalyzemenu">
                            <ul class="navbar-nav ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Works</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Services</a>
                                </li>
                                
                            </ul>
                            <ul class="social-container">
                                <li> <a href="" class="social-item fb"> <img src="{{asset("icon-fb.svg")}}" alt=""></a> </li>
                                <li><a href="" class="social-item tw"> <img src="{{asset("icon-twitter.svg")}}" alt=""></a></li>
                                <li><a href="" class="social-item gplus"> <img src="{{asset("icon-gplus.svg")}}" alt=""></a></li>
                                <li> <button id="search" class="btn-search"><img src="{{asset("icon-search.svg ")}}" alt="" width='20' heigh='20'></button></li>
                            </ul>
                            <!-- <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form> -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class='top-banner'>
        <div class="top-banner-contiainer">
            @foreach ($latest_news ?? '' as $nws)
                <div class="topbanner-item">
                    <div class="img-container">
                        <img src="{{ asset('upload/'.$nws->image) }}"">
                    </div>
                    <div class="banner-caption-container">
                        <div class="banner-title"><a href="{{ url('news_detail',$nws->id) }}" style="color:#FFFFFF">{{ $nws->title }}</a></div>
                        <div class="banner-description">{!! $nws->detail !!}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <section class="latest-news">
        <div class="container">
            <div class="latestnews-container latest-news-slider">
                @foreach ($news ?? '' as $nws)
                <div class="latest-news-item">
                    <a href="{{ url('news_detail',$nws->id) }}">
                        <div class="img-container">
                            <img src="{{ asset('upload/'.$nws->image) }}"">
                        </div>
                        <div class="meta-info">
                            <span class="date">{{ $nws->created_at }}</span>
                        </div>
                        <div class="latestnews-title">
                        {{ $nws->title }}
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="ourclient">
        <div class="container">
            <div class="ourclient-title">Client</div>
            <div class="client-container">
                <div class="client-item"><img src="{{asset("client-1.png")}}" alt=""></div>
                <div class="client-item"><img src="{{asset("client-2.png")}}" alt=""></div>
                <div class="client-item"><img src="{{asset("client-3.png")}}" alt=""></div>
                <div class="client-item"><img src="{{asset("client-4.png")}}" alt=""></div>
                <div class="client-item"><img src="{{asset("client-5.png")}}" alt=""></div>
                <div class="client-item"><img src="{{asset("client-6.png")}}" alt=""></div>
            </div>
        </div>
    </section>
    <section class="social-media">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="socialmedia-item facebook">
                        <div class="fb-header">
                            <div class="user-info">
                                <div class="fb-username">Catalyze</div>
                                <div class="user-like">6.951 likes</div> 
                            </div>
                            <div class="fb-likebutton">
                                <button>
                                <img src="{{asset("button-fb-follow.svg")}}" alt="" width="71" height="33">
                                </button>
                                
                            </div>
                        </div>
                        <div class="fb-post">
                            <p class="date">11 April 2016</p>
                            <p>Ut tristique non elit nec accumsan. Nunc ullamcorper metus at dui luctus, non cursus odio scelerisque. Nulla imperdiet varius arcu quis faucibus. Sed at consectetur lorem, at semper purus. Sed non ornare lacus, ac vehicula lacus. Proin vehicula eget sem tincidunt finibus.
                            </p>
                            <img src="{{asset("img-news3.jpg")}}" alt="" class="fb-images">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4  col-sm-6">
                    <div class="socialmedia-item twitter">
                        <div class="twitter-header">
                            <div class="user-info">
                                <div class="twitter-username"><img src="{{asset("icon-twitter.svg")}}" alt=""> Catalyze</div>
                                
                            </div>
                            <div class="tw-likebutton">
                                <button>
                                <img src="{{asset("button-tw-follow.svg")}}" alt="" width="71" height="33">
                                </button>
                                
                            </div>
                        </div>
                        <div class="tw-post">
                            <div class="tw-post-item">
                                <img src="{{asset("logo-icon.png")}}" alt="" width="40" class="user-photo">
                                <div class="postuser-info">
                                    <div class="tw-username">@calatyze</div>
                                    <div class="post-time">12 hour</div>
                                </div>
                                <div class="post-content">
                                    Ut tristique non elit nec accumsan. Nunc ullamcorper metus at dui luctus, non cursus odio scelerisque. Nulla imp
                                    <img src="{{asset("img-news3.jpg")}}" alt="" class="tw-images">
                                </div>
                                
                            </div>
                        </div>
                        <div class="tw-post">
                            <div class="tw-post-item">
                                <img src="{{asset("logo-icon.png")}}" alt="" width="40" class="user-photo">
                                <div class="postuser-info">
                                    <div class="tw-username">@calatyze</div>
                                    <div class="post-time">12 hour</div>
                                </div>
                                <div class="post-content">
                                    Ut tristique non elit nec accumsan. Nunc ullamcorper metus at dui luctus, non cursus odio scelerisque. Nulla imp
                                    
                                </div>
                                
                            </div>
                        </div>
                        <div class="tw-post">
                            <div class="tw-post-item">
                                <img src="{{asset("logo-icon.png")}}" alt="" width="40" class="user-photo">
                                <div class="postuser-info">
                                    <div class="tw-username">@calatyze</div>
                                    <div class="post-time">12 hour</div>
                                </div>
                                <div class="post-content">
                                    Ut tristique non elit nec accumsan. Nunc ullamcorper metus at dui luctus, non cursus odio scelerisque. Nulla imp
                                   
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="socialmedia-item instagram">
                        <div class="ig-header">
                            <div class="user-info">
                                <div class="ig-username"><img src="{{asset("logo-ig.png")}}" alt=""> Catalyze</div>
                                
                            </div>
                            <div class="ig-likebutton">
                                <button>
                                <img src="{{asset("button-ig-follow.svg")}}" alt="" width="71" height="33">
                                </button>
                                
                            </div>
                        </div>
                        <div class="ig-post">
                            <div class="ig-img">
                                <a href="">
                                <img src="{{asset("img-ig-1.jpg")}}" alt="">
                                <div class="hover">View in <br>Instagram</div>
                                </a>
                            </div>
                            <div class="ig-img">
                                <a href="">
                                <img src="{{asset("img-ig-2.jpg")}}" alt="">
                                <div class="hover">View in <br>Instagram</div>
                                </a>
                            </div>
                            <div class="ig-img">
                                <a href="">
                                <img src="{{asset("img-ig-3.jpg")}}" alt="">
                                <div class="hover">View in <br>Instagram</div>
                                </a>
                            </div>
                            <div class="ig-img">
                                <a href=""> 
                                <img src="{{asset("img-ig-4.jpg")}}" alt="">
                                <div class="hover">View in <br>Instagram</div>
                                </a>
                            </div>
                            <div class="ig-img">
                                <a href="">
                                <img src="{{asset("img-ig-5.jpg")}}" alt="">
                                <div class="hover">View in <br>Instagram</div>
                                </a>
                            </div>
                            <div class="ig-img">
                                <a href="">
                                <img src="{{asset("img-ig-6.jpg")}}" alt="">
                                <div class="hover">View in <br>Instagram</div>
                                </a>
                            </div>
                            <div class="ig-img">
                                <a href="">
                                <img src="{{asset("img-ig-7.jpg")}}" alt="">
                                <div class="hover">View in <br>Instagram</div>
                                </a>
                            </div>
                            <div class="ig-img">
                                <a href="">
                                <img src="{{asset("img-ig-8.jpg")}}" alt="">
                                <div class="hover">View in <br>Instagram</div>
                                </a>
                            </div> 
                        </div>
                </div>
            </div>
        </div>
    </section>
@extends('layouts.footer')
