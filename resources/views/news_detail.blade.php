@extends('layouts.layouts_fe')
    <header class="site_header">
        <div class="container">
            <div class="top-header">
                <div class="logo-container">
                    <a href="{{ url('/') }}"><img src="{{asset("logo_catalyze.png")}}" alt="catalyze"></a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#catalyzemenu" aria-controls="catalyzemenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button> 
                </div>
                <div class="site-menu_container"> 
                    <nav class="navbar navbar-expand-lg  ">   
                        <div class="collapse navbar-collapse mainmenu" id="catalyzemenu">
                            <ul class="navbar-nav ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Works</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Services</a>
                                </li>
                                
                            </ul>
                            <ul class="social-container">
                                <li> <a href="" class="social-item fb"> <img src="{{asset("icon-fb.svg")}}" alt=""></a> </li>
                                <li><a href="" class="social-item tw"> <img src="{{asset("icon-twitter.svg")}}" alt=""></a></li>
                                <li><a href="" class="social-item gplus"> <img src="{{asset("icon-gplus.svg")}}" alt=""></a></li>
                                <li> <button id="search" class="btn-search"><img src="{{asset("icon-search.svg ")}}" alt="" width='20' heigh='20'></button></li>
                            </ul>
                            <!-- <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form> -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8 ">
            <section class="breadcrumb-custom">
                <ul class="breadcrumb-list">
                    <li class="breadcrumb-item">Home</li>
                    <li class="breadcrumb-item">{{$result[0]->title}}</li>
                </ul>
            </section>
            <section class="blog-content">
                <h1 class='news-title'>{{$result[0]->title}}</h1>
                <div class="news-meta">
                    <span class="author">{{$user[0]->name}}</span> / <span class="date">{{$result[0]->created_at}} </span> / WWF 
                </div>
                <div class="news-topic">
                    <span class="topic-title"> Topics </span><a href="" >Valuing Ecosystem Services</a><a href="" >Reuse and Recycling</a> <a href="" >REDD+</a>
                </div>
                <img src="{{ asset('upload/'.$result[0]->image) }}" alt="" class="img-responsive">
                <div class="page-content">
                    {!! $result[0]->detail !!}
                </div>
                <div class="page-share">
                <span class="share-title"> Share ON </span>
                <a href="#"><img src="{{asset("shareicon-fb.svg")}}" alt="" width="40"></a>
                <a href="#"><img src="{{asset("shareicon-tw.svg")}}" alt="" width="40"></a>
                <a href="#"><img src="{{asset("shareicon-gplus.svg")}}" alt="" width="40"></a>
                <a href="#"><img src="{{asset("shareicon-mail.svg")}}" alt="" width="40"></a>
                <a href="#"><img src="{{asset("shareicon-rss.svg")}}" alt="" width="40"></a>
                </div>
                <hr>
                <div class="author">
                    <div class="row  no-gutters">
                        <div class="col-sm-2">
                            <img src="{{ asset('upload/users/'.$user[0]->image) }}" alt="" style="max-width:90%">
                        </div>
                        <div class="col-sm-10">
                            <div class="author-name"><strong>{{ $user[0]->name }} </strong> <br>
                                <span class="author-job">Public Relations, WWF </span>
                            </div>
                            <div class="author-info">
                            {{ $user[0]->description }}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>
        </div>
        
    </div>
    
@extends('layouts.footer')