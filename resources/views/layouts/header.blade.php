<header class="site_header">
    <div class="container">
        <div class="top-header">
            <div class="logo-container">
                <a href=""><img src="{{asset("logo_catalyze.png")}}" alt="catalyze"></a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#catalyzemenu" aria-controls="catalyzemenu" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button> 
            </div>
            <div class="site-menu_container"> 
                <nav class="navbar navbar-expand-lg  ">   
                    <div class="collapse navbar-collapse mainmenu" id="catalyzemenu">
                        <ul class="navbar-nav ">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Works</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Services</a>
                            </li>
                            
                        </ul>
                        <ul class="social-container">
                            <li> <a href="" class="social-item fb"> <img src="{{asset("icon-fb.svg")}}" alt=""></a> </li>
                            <li><a href="" class="social-item tw"> <img src="{{asset("icon-twitter.svg")}}" alt=""></a></li>
                            <li><a href="" class="social-item gplus"> <img src="{{asset("icon-gplus.svg")}}" alt=""></a></li>
                            <li> <button id="search" class="btn-search"><img src="{{asset("icon-search.svg ")}}" alt="" width='20' heigh='20'></button></li>
                        </ul>
                        <!-- <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form> -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>