@extends('admin.layouts.layout')

@section('content')
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>News</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('news.create') }}">Add News</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="table-responsive">
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Details</th>
                <th width="280px">Actions</th>
            </tr>
            @foreach ($news ?? '' as $nws)
                <tr>
                    <td>{{ $nws->id }}</td>
                    <td>{{ $nws->title }}</td>
                    <td>{{ $nws->detail }}</td>
                    <td>
                        <form action="{{ route('news.destroy',$nws->id) }}" method="POST">

                            <a class="btn btn-info" href="{{ route('news.show',$nws->id) }}">Show</a>

                            <a class="btn btn-primary" href="{{ route('news.edit',$nws->id) }}">Edit</a>

                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    {!! $news ?? ''->links() !!}

@endsection